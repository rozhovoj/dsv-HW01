import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.registry.*;
import java.util.Base64;
import java.util.Objects;

public class HelloClient {
	public static void main(String args[])
		{
		try
		{
			// specify local or remote ip
			String ip = "127.0.0.2";
			int port = 10902;
			Registry reg = LocateRegistry.getRegistry(ip, port);
			// stub creation
			Hello stub = (Hello)reg.lookup("Hello");
			// call the object method - no output
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(System.in));
			String msg = reader.readLine();
			while (!Objects.equals(msg, "stop")) {

				String obtained = stub.encrypt(msg);
				System.out.printf("Obtained: " + obtained + '\n');
				String decryptedResponse = decryptCaesar(obtained);
				System.out.printf("Decrypted: " + decryptedResponse + '\n');
				// call the object method - result is an integer
				msg = reader.readLine();
			}
		}

		catch (Exception e)
		{
			System.out.println("Err: "+e.toString());
		}

	}

	private static String decryptCaesar(String msg) {//https://stackoverflow.com/questions/29226813/simple-encryption-in-java-no-key-password
		StringBuilder tmp = new StringBuilder();
	   	final int OFFSET = 4;
	   	for (int i = 0; i < msg.length(); i++) {
	    	tmp.append((char)(msg.charAt(i) - OFFSET));
	   	}

	   	String reversed = new StringBuffer(tmp.toString()).reverse().toString();
	   	return new String(Base64.getDecoder().decode(reversed));
		}

	}