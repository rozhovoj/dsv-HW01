import java.rmi.RemoteException;
import java.util.Base64;

public class HelloImplementation implements Hello {

	public String encrypt(String msg) { 
		System.out.print(msg);
		return ceasar("Received message: " + msg);
	}

	private String ceasar(String msg) {//https://stackoverflow.com/questions/29226813/simple-encryption-in-java-no-key-password
		String b64encoded = Base64.getEncoder().encodeToString(msg.getBytes());

	    // Reverse the string
	    String reverse = new StringBuffer(b64encoded).reverse().toString();

	    StringBuilder tmp = new StringBuilder();
	    final int OFFSET = 4;
	    for (int i = 0; i < reverse.length(); i++) {
	    	tmp.append((char)(reverse.charAt(i) + OFFSET));
	    }
	    return tmp.toString();
	}

}